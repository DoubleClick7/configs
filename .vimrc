if has('nvim')
    set rtp^=/usr/share/vim/vimfiles/
    let g:python3_host_prog = '/usr/bin/python3'
endif
"set rtp+=~kon5oul/.local/lib/python3.7/site-packages/powerline/bindings/vim
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'honza/vim-snippets'
Plugin 'scrooloose/nerdtree'
Plugin 'neoclide/coc.nvim', {'branch': 'release'}
Plugin 'KabbAmine/vCoolor.vim'
Plugin 'mattn/emmet-vim'
Plugin 'Houl/vim-repmo'
Plugin 'junegunn/fzf.vim'
Plugin 'Yggdroot/indentLine'
Plugin 'wfxr/minimap.vim'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-commentary'
Plugin 'sirver/ultisnips'
Plugin 'dylanaraps/wal.vim'
Plugin 'vifm/vifm.vim'
Plugin 'ap/vim-css-color'
Plugin 'vifm/vifm'
Plugin 'matze/vim-move'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'lepture/vim-jinja'
Plugin 'prettier/vim-prettier'
Plugin 'guns/xterm-color-table.vim'
call vundle#end()
filetype plugin on
filetype indent on
"packadd! dracula
syntax enable
colorscheme wal
hi Pmenu            ctermfg=251     ctermbg=234     cterm=none
hi PmenuSel         ctermfg=0       ctermbg=111     cterm=none
hi PmenuSbar        ctermfg=206     ctermbg=235     cterm=none
hi PmenuThumb       ctermfg=235     ctermbg=206     cterm=none
hi VertSplit cterm=NONE ctermbg=NONE


 " ____  _____ _____ ____
" / ___|| ____|_   _/ ___|
" \___ \|  _|   | | \___ \
 " ___) | |___  | |  ___) |
" |____/|_____| |_| |____/

set tabstop=4
set softtabstop=4
set shiftwidth=4
set laststatus=2
set foldmethod=indent
set encoding=utf-8
set hlsearch 
set incsearch
set nocompatible
set expandtab
set nu relativenumber
set showcmd
set ic
set sc
set updatetime=100










"  _     _____ _____ ____
" | |   | ____|_   _/ ___|
" | |   |  _|   | | \___ \
" | |___| |___  | |  ___) |
" |_____|_____| |_| |____/


let mapleader = " "
" Max line length that prettier will wrap on: a number or 'auto' (use
" textwidth).
" default: 'auto'
let g:prettier#config#print_width = 'auto'

" number of spaces per indentation level: a number or 'auto' (use
" softtabstop)
" default: 'auto'
let g:prettier#config#tab_width = 'auto'

" use tabs instead of spaces: true, false, or auto (use the expandtab setting).
" default: 'auto'
let g:prettier#config#use_tabs = 'false'

" flow|babylon|typescript|css|less|scss|json|graphql|markdown or empty string
" (let prettier choose).
" default: ''
let g:prettier#config#parser = ''

" cli-override|file-override|prefer-file
" default: 'file-override'
let g:prettier#config#config_precedence = 'file-override'

" always|never|preserve
" default: 'preserve'
let g:prettier#config#prose_wrap = 'preserve'

" css|strict|ignore
" default: 'css'
let g:prettier#config#html_whitespace_sensitivity = 'css'

" false|true
" default: 'false'
let g:prettier#config#require_pragma = 'false'

let g:indentLine_char = ''
"let g:ycm_global_ycm_extra_conf = '~/.config/nvim/bundle/YouCompleteMe/.ycm_extra_conf.py'

let g:airline_section_b = '%{strftime("%c")}'
let g:airline_section_y = 'BN: %{bufnr("%")}'
let g:airline_theme = 'wal'
let g:airline_powerline_fonts = 1
let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ }
let g:user_emmet_leader_key='<leader>'
let g:user_emmet_mode='n'










"  __  __    _    ____  ____
" |  \/  |  / \  |  _ \/ ___|
" | |\/| | / _ \ | |_) \___ \
" | |  | |/ ___ \|  __/ ___) |
" |_|  |_/_/   \_\_|   |____/

map <C-h> <C-w>h
map <C-l> <C-w>l
map <C-j> <C-w>j
map <C-k> <C-w>k
nnoremap <silent><leader>\ :noh<Return>
nnoremap <a-j> :m .+1<cr>==
nnoremap <a-k> :m .-2<cr>==
inoremap <a-j> <esc>:m .+1<cr>==gi
inoremap <a-k> <esc>:m .-2<cr>==gi
vnoremap <a-j> :m '>+1<cr>gv=gv
vnoremap <a-k> :m '<-2<cr>gv=gv
nnoremap <C-_> :Commentary<CR>
vnoremap <c-_> :Commentary<CR>
inoremap <C-_> <Esc>:Commentary<CR>i
inoremap <C-Return> <CR><CR><C-o>k<Tab>


" map a motion and its reverse motion:
:noremap <expr> h repmo#SelfKey('h', 'l')|sunmap h
:noremap <expr> l repmo#SelfKey('l', 'h')|sunmap l

" if you like `:noremap j gj', you can keep that:
:map <expr> j repmo#Key('gj', 'gk')|sunmap j
:map <expr> k repmo#Key('gk', 'gj')|sunmap k

" repeat the last [count]motion or the last zap-key:
:map <expr> ; repmo#LastKey(';')|sunmap ;
:map <expr> , repmo#LastRevKey(',')|sunmap ,

" add these mappings when repeating with `;' or `,':
:noremap <expr> f repmo#ZapKey('f')|sunmap f
:noremap <expr> F repmo#ZapKey('F')|sunmap F
:noremap <expr> t repmo#ZapKey('t')|sunmap t
:noremap <expr> T repmo#ZapKey('T')|sunmap T

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
